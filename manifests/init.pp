#  $realms_additional
#
#  $other_sections: Additional sections that are formatted as key-value pairs (hash).
#    Example:
#      krb5conf::others_sections:
#        kdc:
#          log_file: SYSLOG:NOTICE
#          acl_file: /etc/heimdal-kdc/kadmind.acl
#          kdc_warn_pwexpire: 5d
#
#    Default = {}
#
#
define krb5conf (
  Optional[String] $header_comment          = undef,
  #
  Optional[Hash]   $libdefaults             = {},
  Optional[Hash]   $libdefaults_additional  = {},
  #
  Optional[Hash]   $realms                  = {},
  Optional[Hash]   $realms_additional       = {},
  #
  Optional[Hash]   $domain_realm            = {},
  Optional[Hash]   $domain_realm_additional = {},
  #
  Optional[Hash]   $logging                 = {},
  Optional[Hash]   $logging_additional      = {},
  #
  Optional[String] $appdefaults_file        = undef,
  Optional[String] $appdefaults_string      = undef,
  #
  Optional[Hash]   $other_sections          = {},
  Optional[Array]  $other_section_files     = [],
) {

  if ($appdefaults_file and $appdefaults_string) {
    crit("only one of 'appdefaults_file' and 'appdefaults_string' can be defined")
  }

  # If the $libdefaults parameter is not defined, read in the default.
  if (size($libdefaults) > 0) {
    $r_libdefaults = $libdefaults
  } else {
    $libdefaults_yaml = file('krb5conf/libdefaults_default')
    $r_libdefaults    = parseyaml($libdefaults_yaml) + $libdefaults_additional
  }

  # If the $realms parameter is not defined, read in the default.
  if (size($realms) > 0) {
    $r_realms = $realms
  } else {
    $realms_yaml = file('krb5conf/realms_default')
    $r_realms = parseyaml($realms_yaml) + $realms_additional
  }

  # If the $domain_realm parameter is not defined, read in the default.
  if (size($domain_realm) > 0) {
    $r_domain_realm = $domain_realm
  } else {
    $domain_realm_yaml = file('krb5conf/domain_realm_default')
    $r_domain_realm    = parseyaml($domain_realm_yaml) + $domain_realm_additional
  }

  # If the $logging parameter is not defined, read in the default.
  if (size($logging) > 0) {
    $r_logging = $logging
  } else {
    $logging_yaml = file('krb5conf/logging_default')
    $r_logging    = parseyaml($logging_yaml) + $logging_additional
  }

  if ($appdefaults_file) {
    $r_appdefaults_string = file($appdefaults_file)
  } elsif ($appdefaults_string) {
    $r_appdefaults_string = $appdefaults_string
  } else {
    $r_appdefaults_string = ""
  }

  $libdefaults_string  = krb5conf::simple_hash_section($r_libdefaults, 'libdefaults')
  $realms_string       = krb5conf::realms($r_realms)
  $domain_realm_string = krb5conf::simple_hash_section($r_domain_realm, 'domain_realm')
  $logging_string      = krb5conf::simple_hash_section($r_logging, 'logging')

  $other_section_strings =
    $other_sections.map |$section_name, $section_hash| { krb5conf::simple_hash_section($section_hash, $section_name) }

  $other_section_files_strings =
    $other_section_files.map |$other_section_file| { file($other_section_file) }
  $other_section_files_string =
    $other_section_files_strings.reduce |$memo, $value| { "${memo}\n${value}" }

  # Generate the header.
  if ($header_comment) {
    $header_comment_string = krb5conf::comment_string($header_comment)
  } else {
    $header_comment_string = template('krb5conf/header_default.erb')
  }

  $all_sections = [$header_comment_string] +
  $other_section_strings +
  [
    $r_appdefaults_string,
    $libdefaults_string,
    $realms_string,
    $domain_realm_string,
    $other_section_files_string,
    $logging_string,
  ]

  $complete_string = $all_sections.reduce |$memo, $value| { "${memo}\n${value}" }

  file { $name:
    content => $complete_string
  }
}
