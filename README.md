<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [The `krb5conf` Puppet Module](#the-krb5conf-puppet-module)
  - [Introduction](#introduction)
  - [Quick Start](#quick-start)
  - [Customization](#customization)
    - [Customizing the `[realms]` section](#customizing-the-realms-section)
    - [Customizing the `[libdefaults]` section](#customizing-the-libdefaults-section)
    - [Customizing the `[appdefaults]` section](#customizing-the-appdefaults-section)
    - [Changing the header comment](#changing-the-header-comment)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# The `krb5conf` Puppet Module

## Introduction

The `krb5conf` Puppet module helps to set up a `krb5.conf` file. The
module contains a `define` so that you can set up multiple `krb5.conf`
files if needed. The `krb5.conf` file created will be appropriate for
Stanford, but can be altered for other environments.

## Quick Start

To set up `/etc/krb5.conf` for a Stanford server:

    krb5conf { '/etc/krb5.conf':
      appdefaults_file => 'krb5conf/appdefaults',
    }

The file `/etc/krb5.conf` will contain the sections

    [appdefaults]
    [libdefaults]
    [realms]
    [domain_realm]
    [logging]

The `stanford.edu` entry in the `[realms]` section will be
configured to point to the correct Stanford KDC's in the recommended
order.

The `[libdefaults]` section will contain these settings:

    default_realm     = stanford.edu
    default_lifetime  = 25hrs
    krb4_convert      = false
    ticket_lifetime   = 25h
    renew_lifetime    = 7d
    forwardable       = true
    noaddresses       = true
    rdns              = true

## Customization

You can customize each of the sections `[appdefaults]`, `[libdefaults]`,
`[realms]`, `[domain_realm]`, `[logging]`.

### Customizing the `[realms]` section

By default, `krb5conf` reads in the YAML file `krb5conf/realms_default`
which contains realms appropriate for Stanford. If you want to use your
own set of realms, use the `realms` parameter.

For example:

    krb5conf { '/etc/krb5.conf':
      appdefaults_file => 'krb5conf/appdefaults',
      realms => {
        'EXAMPLE.COM' => [
          ['kdc', 'kdc1.example.com'],
          ['kdc', 'kdc2.example.com'],
          ['kpasswd_server', 'kdc1.example.com'],
          ['default_domain', 'example.com'],
        ],
        'MYDOMAIN.COM' => [
          ['kdc', 'kdc1.mydomain.com'],
          ['kpasswd_server', 'admin.mydomain.com'],
          ['default_domain', 'mydomain.com'],
        ]
    }

creates the `[realms]` section with only these two sections:

    EXAMPLE.COM = {
        kdc            = kdc1.example.com
        kdc            = kdc2.example.com
        kpasswd_server = kdc1.example.com
        default_domain = example.com
    }
    MYDOMAIN.COM = {
        kdc            = kdc1.mydomain.com
        kpasswd_server = admin.mydomain.com
        default_domain = mydomain.com
    }

If you want to _replace_ one of the realms provided by the default
configuration, simply do the same as above. For example, to change the
`stanford.edu` realm to add a fourth KDC you would do something like this:

    krb5conf { '/tmp/newstanford.conf':
      appdefaults_file => 'krb5conf/appdefaults',
      realms => {
        'stanford.edu' => [
          ['kdc', 'krb5auth1.stanford.edu'],
          ['kdc', 'krb5auth2.stanford.edu'],
          ['kdc', 'krb5auth3.stanford.edu'],
          ['kdc', 'krb5auth4.stanford.edu'],
          ['master_kdc',     'master-kdc.stanford.edu:88'],
          ['admin_server',   'krb5-admin.stanford.edu'],
          ['kpasswd_server', 'krb5-admin.stanford.edu'],
          ['default_domain', 'stanford.edu'],
          ['kadmind_port',   '749']
        ]
      }
    }

### Customizing the `[libdefaults]` section

If you want to use your own `[libdefaults]` settings replacing the ones
that come by default, use the `libdefaults` parameter. For example:

    krb5conf { '/etc/krb5.conf':
      libdefaults => { 'default_realm' => 'example.com' },
    }

Will result in a `[libdefaults]` section containing _only_ the
`default_realm` setting.

If you want to _replace_ or _add to_ one of the parameters in the
`[libdefaults]` section, use `libdefaults_additional`. In the following
example, you will get the usual `[libdefaults]` settings but the `rdns`
setting will be set to `true`.

    # Change "rdns" from the recommended "false" to "true"
    krb5conf { '/etc/krb5.conf':
      libdefaults_additional => { 'rdns' => 'true' },
    }

### Customizing the `[appdefaults]` section

The `[appdefaults]` section can only be overridden in total. You do this
with either a file or a string. Example:

    $new_appdefaults = @("NEWAPPDEFAULTS"/L)
    [appdefaults]
        default_lifetime  = 25hrs
        krb4_convert      = false
        krb4_convert_524  = false

        wallet = {
            wallet_server = wallet.stanford.edu
        }
    | NEWAPPDEFAULTS

    krb5conf { '/tmp/newstanford.conf':
      libdefaults    => { 'default_realm' => 'example.com' },
      appdefaults_string => $new_appdefaults,
    }

The above will result in this `[appdefaults]`:

    [appdefaults]
        default_lifetime  = 25hrs
        krb4_convert      = false
        krb4_convert_524  = false

        wallet = {
            wallet_server = wallet.stanford.edu
        }

**Note.** If you override the `[appdefaults]` you must provide the literal
  string "[appdefaults]" at the top of the section.

### Changing the header comment

The `krb5conf` define will put a comment at the top of the file indicating
that the file was created by the `krb5conf` module. You change this comment by
using the `header_comment` parameter. For example:

    krb5conf { '/etc/krb5.conf':
      header_comment => "Changing rdns to true\nfor application compatibility reasons.",
      libdefaults    => { 'default_realm' => 'example.com' },
    }

will result in the following comment at the top of the file:

    # Changing rdns to true
    # for application compatibility reasons.
