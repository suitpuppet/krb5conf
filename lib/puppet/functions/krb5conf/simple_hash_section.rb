Puppet::Functions.create_function(:'krb5conf::simple_hash_section') do
  def simple_hash_section(hash, title)
    indent = 4
    spacer = ' ' * indent

    lengths = hash.keys().map { |x| x.length() }
    max_length = lengths.max
    rv = []
    hash.each do |key, value|
      if (key == 'COMMENT') then
        # Add a '#' mark at the start of each line.
        formatted_lines = value.split("\n").map { |l| spacer + '# ' + l }
        rv.push(formatted_lines)
      else
        rv.push(sprintf("#{spacer}%-#{max_length}s = %s", key, value))
      end
    end
    return "[#{title}]\n" + rv.join("\n") + "\n"
  end
end
