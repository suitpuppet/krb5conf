Puppet::Functions.create_function(:'krb5conf::realms') do
  def realms(realms_hash)
    indent = 4
    spacer = ' ' * indent

    final_string = ""
    list_of_realms = realms_hash.keys()
    list_of_realms.each do |r|
      rv = []
      attributes = realms_hash[r]
      lengths    = attributes.map { |x| x[0].length() }
      max_length = lengths.max
      attributes.each do |attribute|
        rv.push(sprintf("#{spacer}#{spacer}%-#{max_length}s = %s", attribute[0], attribute[1]))
      end
      final_string += "#{spacer}#{r} = {\n" + rv.join("\n") + "\n#{spacer}}\n"
    end
    return "[realms]\n#{final_string}"
  end
end
