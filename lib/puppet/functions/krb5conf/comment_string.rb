Puppet::Functions.create_function(:'krb5conf::comment_string') do
  def comment_string(str)
    final_string = ""
    return str.split("\n").map { |line| "# " + line }.join("\n")
  end
end
